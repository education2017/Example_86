package hr.ferit.bruno.example_86;

import android.os.AsyncTask;
import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Zoric on 29.8.2017..
 */

public class GetCountryInfoTask extends AsyncTask<String, Double, String> {

    private static String BASE_URL = "https://restcountries.eu/rest/v2/name/";
    private ICountryInfoDisplay mDisplay;

    public GetCountryInfoTask(ICountryInfoDisplay display){
        this.mDisplay = display;
    }

    public void detach(){
        this.mDisplay = null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(this.mDisplay != null){
            this.mDisplay.prepareDisplay();
        }
    }

    @Override
    protected void onProgressUpdate(Double... values) {
        super.onProgressUpdate(values);
        if(this.mDisplay != null){
            this.mDisplay.updateProgress(values[0]);
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(this.mDisplay != null){
            this.mDisplay.displayResult(result);
        }
    }

    @Override
    protected String doInBackground(String... query) {

        String countryInfo = "";

        for (int i=0;i<query.length;i++){
            String countryUrl = BASE_URL + query[i];
            countryInfo += retrieveCountryInfo(countryUrl);
            double percentageProgress = ((double)i) / query.length * 100;
            publishProgress(percentageProgress);
        }

        return countryInfo;
    }

    private String retrieveCountryInfo(String countryUrl) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder().url(countryUrl).build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
