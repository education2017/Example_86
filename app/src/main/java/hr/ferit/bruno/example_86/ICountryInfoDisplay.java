package hr.ferit.bruno.example_86;

/**
 * Created by Zoric on 29.8.2017..
 */

public interface ICountryInfoDisplay {
    void updateProgress(double progress);
    void displayResult(String result);
    void prepareDisplay();
}
