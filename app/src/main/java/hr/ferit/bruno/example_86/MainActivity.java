package hr.ferit.bruno.example_86;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends Activity implements ICountryInfoDisplay {

    @BindView(R.id.etCountryInput) EditText etCountryInput;
    @BindView(R.id.tvCountryInfoDisplay) TextView tvCountryInfoDisplay;
    @BindView(R.id.bGetCountryInfo) Button bGetCountryInfo;
    @BindView(R.id.pbProgress) ProgressBar pbProgress;

    GetCountryInfoTask mTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(this.mTask != null){
            this.mTask.detach();
        }
    }

    @OnClick(R.id.bGetCountryInfo)
    public void getCountryInfo(){
        String query = this.etCountryInput.getText().toString();
        this.mTask = new GetCountryInfoTask(this);
        this.mTask.execute(query);
    }

    @Override
    public void updateProgress(double progress) {
        Log.d("TAG", String.valueOf(progress));
        pbProgress.setProgress((int)progress);
    }

    @Override
    public void displayResult(String result) {
        pbProgress.setVisibility(View.GONE);
        tvCountryInfoDisplay.setText(result);
    }

    @Override
    public void prepareDisplay() {
        tvCountryInfoDisplay.setText("");
        pbProgress.setVisibility(View.VISIBLE);
        Toast.makeText(this,"Started task", Toast.LENGTH_SHORT).show();
    }
}
